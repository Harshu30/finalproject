package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.TechnicalDao;
import com.model.Technical;


@RestController
public class TechnicalController {
	@Autowired
    private TechnicalDao technicalDao;

    @GetMapping("random-technicals")
    public List<Technical> getRandomTechnicals(@RequestParam int limit) {
        return technicalDao.getRandomTechnicals(limit);
    }
}

