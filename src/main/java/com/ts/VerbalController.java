package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.VerbalDao;
import com.model.Verbal;


@RestController
public class VerbalController {
	@Autowired
    private VerbalDao verbalDao;

    @GetMapping("random-verbals")
    public List<Verbal> getRandomVerbals(@RequestParam int limit) {
        return verbalDao.getRandomVerbals(limit);
    }
}

