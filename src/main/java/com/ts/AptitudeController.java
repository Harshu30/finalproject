package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AptitudeDao;
import com.model.Aptitude;

@RestController
public class AptitudeController {
	@Autowired
    private AptitudeDao aptitudeDao;

    @GetMapping("random-aptitudes")
    public List<Aptitude> getRandomAptitudes(@RequestParam int limit) {
        return aptitudeDao.getRandomAptitudes(limit);
    }
}
