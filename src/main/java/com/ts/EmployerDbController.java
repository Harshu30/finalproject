package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployerDbDao;
import com.model.EmployerDb;



@CrossOrigin(origins="http://localhost:4200")
@RestController
public class EmployerDbController {
	
	@Autowired
	EmployerDbDao employerDbDao;

	@GetMapping("getEmployerDbs")
	public List<EmployerDb> getEmployerDbs() {
		return employerDbDao.getEmployerDbs();
	}
	
	
	@GetMapping("getEmployerDbById/{EmployerDbId}")
	public EmployerDb getEmployerDbById(@PathVariable int EmployerDbId) {
		return employerDbDao.getEmployerDbById(EmployerDbId);
	}
	
	@GetMapping("getEmployerDbByName/{EmployerDbName}")
	public EmployerDb getEmployerDbByName(@PathVariable String EmployerDbName) {
		return employerDbDao.getEmployerDbByName(EmployerDbName);
	}
	
	@PostMapping("addEmployerDb")
	public EmployerDb addEmployerDb(@RequestBody EmployerDb EmployerDb) {
		return employerDbDao.addEmployerDb(EmployerDb);
	}
	
	@PutMapping("updateEmployerDb")
	public EmployerDb updateEmployerDb(@RequestBody EmployerDb EmployerDb) {
		return employerDbDao.updateEmployerDb(EmployerDb);
	}
	
	@DeleteMapping("deleteEmployerDbById/{EmployerDbId}")
	public String deleteEmployerDbById(@PathVariable int EmployerDbId) {
		employerDbDao.deleteEmployerDbById(EmployerDbId);
		return "EmployerDb With EmployerDbId:" + EmployerDbId + " Deleted Successfully!!!" ;
	}
}


