package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployerDb {
	
	@Id
	@GeneratedValue
	private int jobId;
	private String cName;
	private String jobTitle;
	private String jobNature;
	private String workPlace;
	private String workLocation;
	private String testTopic;
	private double salary;
	private String jobDescription;
	
	public EmployerDb(){
		
	}

	public EmployerDb(String cName, String jobTitle, String jobNature, String workPlace, String workLocation,
			String testTopic, double salary, String jobDescription) {
		super();
		this.cName = cName;
		this.jobTitle = jobTitle;
		this.jobNature = jobNature;
		this.workPlace = workPlace;
		this.workLocation = workLocation;
		this.testTopic = testTopic;
		this.salary = salary;
		this.jobDescription = jobDescription;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobNature() {
		return jobNature;
	}

	public void setJobNature(String jobNature) {
		this.jobNature = jobNature;
	}

	public String getWorkPlace() {
		return workPlace;
	}

	public void setWorkPlace(String workPlace) {
		this.workPlace = workPlace;
	}

	public String getWorkLocation() {
		return workLocation;
	}

	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}

	public String getTestTopic() {
		return testTopic;
	}

	public void setTestTopic(String testTopic) {
		this.testTopic = testTopic;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	
	
	
}