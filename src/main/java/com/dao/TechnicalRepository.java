package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Technical;


@Repository
public interface TechnicalRepository extends JpaRepository<Technical, Long> {

    @Query(value = "SELECT * FROM technical ORDER BY RAND() LIMIT :limit", nativeQuery = true)
    List<Technical> getRandomTechnicals(@Param("limit") int limit);

}
