package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.EmployerDb;


@Service
public class EmployerDbDao {
	
	@Autowired
	EmployerDbRepository employerDbRepository;

	public List<EmployerDb> getEmployerDbs() {
		return employerDbRepository.findAll();
	}

	public EmployerDb getEmployerDbById(int EmployerDbId) {
		return employerDbRepository.findById(EmployerDbId).orElse(null);
	}

	public EmployerDb getEmployerDbByName(String EmployerDbName) {
		return employerDbRepository.findByName(EmployerDbName);
	}

	public EmployerDb addEmployerDb(EmployerDb EmployerDb) {
		return employerDbRepository.save(EmployerDb);
	}

	public EmployerDb updateEmployerDb(EmployerDb EmployerDb) {
		return employerDbRepository.save(EmployerDb);
	}

	public void deleteEmployerDbById(int EmployerDbId) {
		employerDbRepository.deleteById(EmployerDbId);
	}


	
}

