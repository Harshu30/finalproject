package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Technical;


@Service
public class TechnicalDao {
	@Autowired
    private TechnicalRepository technicalRepository;

    public List<Technical> getRandomTechnicals(int limit) {
        return technicalRepository.getRandomTechnicals(limit);
    }
}
