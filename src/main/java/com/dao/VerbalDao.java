package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Verbal;


@Service
public class VerbalDao {
	@Autowired
    private VerbalRepository verbalRepository;

    public List<Verbal> getRandomVerbals(int limit) {
        return verbalRepository.getRandomVerbals(limit);
    }
}
