package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Verbal;


@Repository
public interface VerbalRepository extends JpaRepository<Verbal, Long> {

	    @Query(value = "SELECT * FROM verbal ORDER BY RAND() LIMIT :limit", nativeQuery = true)
	    List<Verbal> getRandomVerbals(@Param("limit") int limit);
}
