package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Aptitude;

@Repository
public interface AptitudeRepository extends JpaRepository<Aptitude, Long> {

    @Query(value = "SELECT * FROM aptitude ORDER BY RAND() LIMIT :limit", nativeQuery = true)
    List<Aptitude> getRandomAptitudes(@Param("limit") int limit);

}
