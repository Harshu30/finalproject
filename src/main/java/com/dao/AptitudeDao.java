package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Aptitude;

@Service
public class AptitudeDao {
	@Autowired
    private AptitudeRepository aptitudeRepository;

    public List<Aptitude> getRandomAptitudes(int limit) {
        return aptitudeRepository.getRandomAptitudes(limit);
    }
}
