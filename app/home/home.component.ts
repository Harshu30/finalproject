import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; // Import Router module

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) {} // Inject Router module

  ngOnInit() {}

  employerLogin() {
    this.router.navigate(['/employer']); // Navigate to the Employer page
  }
  jobSeeker() {
    this.router.navigate(['/employer-login']); // Navigate to the Employer page
  }
}
