import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css'],
})
export class EventFormComponent {
  event: any = {}; // Define the type of event object

  constructor(private http: HttpClient) {}

  saveEvent() {
    this.http.post('/api/events', this.event).subscribe(() => {
      // Success notification or redirect
    });
  }
}
