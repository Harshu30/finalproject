// app.module.ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module'; // Import your AppRoutingModule
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { JobSeekerComponent } from './jobseeker/job-seeker.component';
import { EmployerDashboardComponent } from './employer-dashboard/employer-dashboard.component';
import { EmployerComponent } from './employer/employer.component';
import { EmployerLoginComponent } from './employer-login/employer-login.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    JobSeekerComponent,
    EmployerDashboardComponent,
    EmployerComponent,
    EmployerLoginComponent,
    // other components...
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule, // Import your AppRoutingModule here
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
