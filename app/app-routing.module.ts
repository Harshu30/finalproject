import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { authGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { JobSeekerComponent } from './jobseeker/job-seeker.component';
import { EmployerComponent } from './employer/employer.component';
import { FormsModule } from '@angular/forms';
import { EmployerLoginComponent } from './employer-login/employer-login.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'employer-login',
    component: EmployerLoginComponent,
  },
  { path: 'employer', component: EmployerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
