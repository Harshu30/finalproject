import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-job-seeker',
  templateUrl: './job-seeker.component.html',
  styleUrl: './job-seeker.component.css',
})
export class JobSeekerComponent {
  username: string = '';
  mobile: string = '';
  qualification: string = '';
  college: string = '';
  email: string = '';
  password: string = '';

  constructor(private http: HttpClient) {}
  save() {
    let bodyData = {
      username: this.username,
      mobile: this.mobile,
      qualification: this.qualification,
      college: this.college,
      email: this.email,
      password: this.password,
    };

    let bodyEmailData = {
      subject: 'TalentTrek',
      message: 'Thankyou for registering',
    };
    this.http
      .post('http://localhost:8080/api/v1/user/save', bodyData, {
        responseType: 'text',
      })
      .subscribe((resultData: any) => {
        console.log(resultData);
        alert('JobSeeker registered Successfully');
      });
  }
}
