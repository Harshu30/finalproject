import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employer',
  templateUrl: './employer.component.html',
  styleUrl: './employer.component.css',
})
export class EmployerComponent {
  company: string = '';
  password: string = '';
  confirmPassword: string = '';
  passwordError: string = '';
  confirmPasswordError: string = '';
  emp: any;

  constructor(private service: EmpService, private router: Router) {
    this.emp = {
      cName: '',
      empName: '',
      email: '',
      mobile: '',
      password: '',
    };
  }

  validatePassword(): void {
    this.passwordError = '';
    if (!this.password || this.password.length < 8) {
      this.passwordError = 'Password should be at least 8 characters long.';
    } else if (!/[A-Z]/.test(this.password)) {
      this.passwordError = 'Please add at least one uppercase letter.';
    } else if (!/[a-z]/.test(this.password)) {
      this.passwordError = 'Please add at least one lowercase letter.';
    } else if (!/\d/.test(this.password)) {
      this.passwordError = 'Please add at least one digit.';
    } else if (!/[@$!%*?&]/.test(this.password)) {
      this.passwordError = 'Please add at least one special character.';
    }
  }

  validateConfirmPassword(): void {
    this.confirmPasswordError = '';
    if (this.password !== this.confirmPassword) {
      this.confirmPasswordError = 'Passwords do not match.';
    }
  }

  registerSubmit(regForm: any) {
    console.log(regForm);

    this.emp.cName = regForm.cName;
    this.emp.empName = regForm.empName;
    this.emp.email = regForm.email;
    this.emp.mobile = regForm.mobile;
    this.emp.password = regForm.password;

    console.log(this.emp);

    this.service.registerEmployer(this.emp).subscribe((data: any) => {
      console.log(data);
    });
    alert('success');
  }
}
