import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-employer-login',
  templateUrl: './employer-login.component.html',
  styleUrl: './employer-login.component.css',
})
export class EmployerLoginComponent implements OnInit {
  emp: any;

  constructor(private router: Router, private service: EmpService) {}

  ngOnInit() {}

  async loginSubmit(loginForm: any) {
    this.emp = null;

    await this.service
      .employerLogin(loginForm.email, loginForm.password)
      .then((data: any) => {
        console.log(data);
        this.emp = data;
      });

    if (this.emp != null) {
      alert('Invalid Credentials');
    } else {
      alert('success');
    }
  }
}
