import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EmpService {
  employerLogin(email: any, password: any): any {
    return this.http
      .get('http://localhost:8085/empLogin/' + email + '/' + password)
      .toPromise();
  }
  constructor(private http: HttpClient) {}
  registerEmployer(employer: any): any {
    return this.http.post('http://localhost:8085/addEmployer', employer);
  }
}
