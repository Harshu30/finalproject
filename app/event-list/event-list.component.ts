import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css'],
})
export class EventListComponent implements OnInit {
  events: any[] = []; // Initialize events array

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getEvents();
  }

  getEvents() {
    this.http.get<any[]>('/api/events').subscribe((events) => {
      this.events = events;
    });
  }
}
