import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  getProductDetails(productName: string): Observable<ProductDetails> {
    const apiUrl = `your_api_endpoint/${productName}`;

    return this.http.get<ProductDetails>(apiUrl);
  }
}

// Assuming ProductDetails is a type representing the structure of your product details
interface ProductDetails {
  name: string;
  description: string;
  price: number;
  image: string;
  // Add other properties as needed
}
