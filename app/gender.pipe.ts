import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender',
})
export class GenderPipe implements PipeTransform {
  transform(empName: any, gender: string): any {
    if (gender === 'Male') {
      return 'Mr. ' + empName;
    } else {
      return 'Miss.' + empName;
    }
  }
}
